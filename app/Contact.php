<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['user_one_id', 'user_two_id', 'room_id'];

    public function user_one()
    {
        return $this->belongsTo(User::class);
    }

    public function user_two()
    {
        return $this->belongsTo(User::class);
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }


}

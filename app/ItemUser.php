<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemUser extends Model
{
    protected $fillable = [
        'user_id','item_id','quantity'
    ];
}

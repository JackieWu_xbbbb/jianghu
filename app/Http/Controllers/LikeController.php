<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\Request;
use Auth;

class LikeController extends Controller
{
    public function post(Request $request){
        $user=Auth::user();
        $like=new Like();
        $like->user_id=$user->id;
        $like->hobby_id=$request->input('hobby_id');
        $like->save();
        return response()->json(['message' => 'success'],200);
    }
}

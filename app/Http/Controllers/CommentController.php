<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Auth;

class CommentController extends Controller
{
    public function post(Request $request){
        $user=Auth::user();
        $comment=new Comment();
        $comment->content=$request->input('content');
        $comment->user_id=$user->id;
        $comment->hobby_id=$request->input('hobby_id');
        $comment->target_user=$request->input('target_user');
        $comment->save();
        return response()->json(['message' => 'success'],200);
    }


}

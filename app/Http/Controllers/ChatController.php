<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Room, User, Contact, Chatkit};
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class ChatController extends Controller
{

    public function get_all_chat(){
        $user=Auth::user();
        $contacts=Contact::where('user_one_id',$user->id)->orWhere('user_two_id',$user->id)->get();
        Log::debug(json_encode($contacts)) ;
        $results=array();
        foreach ($contacts as $contact){
            if($contact->user_one_id!=$user->id){
                $friend=User::find($contact->user_one_id);
                $results[]=$friend;
                continue;
            }
            if($contact->user_two_id!=$user->id){
                $friend=User::find($contact->user_two_id);
                $results[]=$friend;
                continue;
            }
        }
        return response()->json($results,200);
    }

    public function create_one_to_one_chat(Request $request, Chatkit $chatkit){
        $contact=Contact::where('user_one_id',$request->get('user_one_id'))->where('user_two_id',$request->get('user_two_id'))->first();
        $contact_2=Contact::where('user_one_id',$request->get('user_two_id'))->where('user_two_id',$request->get('user_one_id'))->first();
        if($contact!=null&&$contact_2==null){
            return response()->json(['message' => 'success'],200);
        }
        else{
            $user_1=User::find($request->get('user_one_id'));
            $user_2=User::find($request->get('user_two_id'));
            $response = $chatkit->createRoom([
                'creator_id' => env('CHATKIT_USER_ID'),
                'private' => true,
                'name' => $this->generate_room_id($user_1->chatkit_id,$user_2->chatkit_id),
                'user_ids' => [$user_1->chatkit_id, $user_2->chatkit_id],
            ]);
            if ($response['status'] !== 201) {
                return response()->json(['status' => 'error'], 400);
            }
            else{
                $room = Room::create($response['body']);
                Log::debug($room->id);
                $contact = Contact::create([
                    'user_one_id' => $user_1->id,
                    'user_two_id' => $user_2->id,
                    'room_id' => $room->id
                ]);
                return response()->json(['message' => 'success'],200);
            }
        }
    }




    private function generate_room_id($user_one_id, $user_two_id) : string
    {
        $chatkit_ids = [$user_one_id, $user_two_id];
        sort($chatkit_ids);
        return md5(implode('', $chatkit_ids));
    }
}

<?php

namespace App\Http\Controllers;

use App\Collection;
use Illuminate\Http\Request;
use App\Category;
use App\User;
use Auth;
class CollectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function  all(){
        $collections = Collection::all();
        return response()->json($collections,200);
    }
    public function index()
    {
        $collections = Collection::all();
        return view('collections.index',compact('collections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('collections.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $collection = new Collection();
        $collection->name = $request->get('name');
        $collection->save();
        return redirect('collections')->with('success','已添加类别');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        // $category = Category::find($id);
      //  $products = $category->products->sortBy('name');
      //  return view('categories.show',compact('category','products','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $collection = Collection::find($id);
        return view('collections.edit',compact('collection','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $collection = Collection::find($id);

        $collection->name = $request->get('name');

        $collection->save();
        return redirect('collections')->with('success','更新类别成功');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $collection = Collection::find($id);
        if($collection->products()->withTrashed()->get()->isEmpty()){
            $collection->delete();
            return redirect('collections')->with('success', '删除类别成功');
        } else {
            return redirect('collections')->with('fail','请确定已经转移所有该类别下的产品（包括已删除产品）');
        }
    }



    public function order(){
      //  $categories = Category::all();
     //   return view('categories.order',compact('categories'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Category;
use App\Hobby;
use App\Collection;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Storage;

class HobbyController extends Controller
{
    public function post(Request $request){
        $user=Auth::user();
        $hobby=new Hobby();
        $hobby->content=$request->input('content');
        $hobby->user_id=$user->id;
        $hobby->cate_id=$request->cate_id;
        $hobby->collection_id=$request->collection_id;
        $hobby->save();

        if ($request->has('image_url') && $files = $request->get('image_url')) {
            $i=0;
            $images = array();
            foreach ($files as $file_string){
                $file_string=str_replace('data:image/png;base64,', '', $file_string);
                $file_string = str_replace(' ', '+', $file_string);
                $imageName = $hobby->id.'_'.$i.'.png';
                Storage::put( "hobbies_image/". $imageName, base64_decode($file_string));
                $images[] = storage_path(). '/hobbies_image/' . $imageName;
                $i++;
            }
            $hobby->image_url = implode("|", $images);
            $hobby->save();
        }

        return response()->json(['message' => 'success'],200);
    }

    public function all(){
        $hobbies=Hobby::all();
        foreach($hobbies as $hobby){
            $hobby['comments']=$hobby->comments;
        }
        return response()->json($hobbies,200);
    }

    public function get_by_cate_id($id){
        $hobbies=Hobby::where("cate_id",$id)->get();
        foreach($hobbies as $hobby){
            $hobby['comments']=$hobby->comments;
        }
        return response()->json($hobbies,200);
    }


    public function index()
    {
        $hobbies=Hobby::all();
        return view('hobbies.index',compact('hobbies'));
    }


    public function create()
    {
        $collections = Collection::all();
        return view('hobbies.create',compact('collections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hobby=new Hobby();
        $user=Auth::user();
        $hobby->content=$request->get('content');
        if ($request->has('image_url') && $files = $request->file('image_url')) {
            $images = array();

            foreach ($files as $file) {
                if ($file->isValid()) {
                    $img_path = $file->store('hobbies_image');
                    $images[] = $img_path;
                } else {
                    return redirect(route('hobbies'))->with('fail', '上传失败');
                }
            }
            $hobby->image_url = implode("|", $images);
            $hobby->user_id=$user->id;
            $hobby->cate_id=$request->cate_id;
            $hobby->collection_id=$request->collection_id;
            $hobby->save();
            return redirect(action('HobbyController@index'))->with('success','上传成功');
        }

       /* $collection = new Collection();
        $collection->name = $request->get('name');
        $collection->save();
        return redirect('collections')->with('success','已添加类别');*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $category = Category::find($id);
        //  $products = $category->products->sortBy('name');
        //  return view('categories.show',compact('category','products','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hobby=Hobby::find($id);
        $collections=Collection::all();
        $categories=Category::all();
        return view('hobbies.edit',compact('hobby','collections','categories','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hobby=Hobby::find($id);
        $user=Auth::user();
        if ($request->has('image_url') && $files = $request->file('image_url')) {

            if(count($files)>0){
                $images = array();
                foreach ($files as $file) {
                    if ($file->isValid()) {
                        $img_path = $file->store('hobbies_image');
                        $images[] = $img_path;
                    } else {
                        return redirect(route('hobbies'))->with('fail', '上传失败');
                    }
                }
                $hobby->image_url = implode("|", $images);
            }


        }
        $hobby->content=$request->get('content');
        $hobby->user_id=$user->id;
        $hobby->cate_id=$request->cate_id;
        $hobby->collection_id=$request->collection_id;;
        $hobby->save();
        return redirect(action('HobbyController@index'))->with('success','更新成功');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hobby = Hobby::find($id);
        $hobby->comments()->delete();
        $hobby->likes()->delete();
        $hobby->delete();
        return redirect('hobbies')->with('success', '删除成功');

    }

}

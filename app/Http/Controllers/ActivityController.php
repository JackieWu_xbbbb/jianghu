<?php

namespace App\Http\Controllers;

use App\Activity;
use App\ActivityUser;
use App\Collection;
use App\Category;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ActivityController extends Controller
{

    public function search_service($word){
        $service=Activity::where('type',3)->where('title','LIKE','%'.$word.'%')->get();
        return response()->json($service,200);
    }

    public function post(Request $request){
        $user=Auth::user();
        $activity=new Activity();
        $activity->content=$request->get('content');
        $activity->collection_id=$request->get('collection_id');
        $activity->cate_id=$request->get('cate_id');
        $activity->address=$request->get('address');
        $activity->start_time=$request->get('start_time');
        $activity->end_time=$request->get('end_time');
        $activity->title=$request->get('title');
        $activity->type=$request->get('type');
        $activity->user_id=$user->id;
        $activity->save();
        if ($request->has('img_url_top') && $file_string = $request->get('img_url_top')){
            $file_string=str_replace('data:image/png;base64,', '', $file_string);
            $file_string = str_replace(' ', '+', $file_string);
            $imageName = $activity->id.'_top.png';
            Storage::put( "activities_image/".$imageName, base64_decode($file_string));
            $activity->img_url_top= storage_path(). '/activities_image/' . $imageName;
            $activity->save();
        }
        if ($request->has('img_url_bottom') && $file_string = $request->get('img_url_bottom')){
            $file_string=str_replace('data:image/png;base64,', '', $file_string);
            $file_string = str_replace(' ', '+', $file_string);
            $imageName = $activity->id.'_bottom.png';
            Storage::put( "activities_image/".$imageName, base64_decode($file_string));
            $activity->img_url_bottom= storage_path(). '/activities_image/' . $imageName;
            $activity->save();
        }
            return response()->json(['message' => 'success'],200);
    }

    public function all(){
        $activities=Activity::all();
        return response()->json($activities,200);

        //$hobbies=Hobby::all();
        //foreach($hobbies as $hobby){
       //     $hobby['comments']=$hobby->comments;
       // }
    }

    public function get_activity_by_cate_id($id,$type){
        $activities=Activity::where('cate_id',$id)->where('cate_id',$id)->where("type",$type)->get();
        return response()->json($activities,200);
    }

    public function get_activity_by_type($type){
        $activities=Activity::where("type",$type)->get();
        return response()->json($activities,200);
    }




    public function join(Request $request){
        $user=Auth::user();
        $activityUser=new ActivityUser();
        $activityUser->user_id=$user->id;
        $activityUser->activity_id=$request->activity_id;
        $activityUser->save();
        return response()->json(['message' => 'success'],200);
    }

    public function get_joined_activity(){
        $user=Auth::user();
        $activity_users=ActivityUser::where('user_id',$user->id)->get();
        $activity[]=array();
        $i=0;
        foreach ($activity_users as $activity_user){
            $ac=Activity::find($activity_user->activity_id);
            $activity[$i]=$ac;
            $i++;
        }
        return response()->json($activity, 200);
    }

    public function check_joined_activity($user_id,$activity_id){
        $user=Auth::user();
        $activity_users=ActivityUser::where('user_id',$user_id)->where('activity_id',$activity_id)->first();
        if($activity_users){
            $activity=Activity::find($activity_users->activity_id);
            if($activity->user_id==$user->id){
                return response()->json(['message' => 'success'],200);

            }
            else{
                return response()->json(['message' => 'error'],400);

            }
        }
    }

    public function index()
    {
        $activities=Activity::all();
        return view('activities.index',compact('activities'));
    }

    public function create()
    {
        $collections=Collection::all();
        return view('activities.create',compact('collections'));
    }

    public function store(Request $request)
    {
        $user=Auth::user();
        $activity=new Activity();
        $activity->content=$request->get('content');
        $activity->collection_id=$request->get('collection_id');
        $activity->cate_id=$request->get('cate_id');
        $activity->address=$request->get('address');
        $activity->start_time=$request->get('start_time');
        $activity->end_time=$request->get('end_time');
        $activity->title=$request->get('title');
        $activity->type=$request->get('type');
        $activity->user_id=$user->id;
        $activity->address=$request->get('address');
        if($request->has('img_url_top') && $file = $request->file('img_url_top')){
            if ($file->isValid()) {
                $img_path = $file->store('activities_image');
                $activity->img_url_top=$img_path;
            }
        }
        if($request->has('img_url_bottom') && $file = $request->file('img_url_bottom')){
            if ($file->isValid()) {
                $img_path = $file->store('activities_image');
                $activity->img_url_bottom=$img_path;
            }
            else {
                return redirect(route('activities'))->with('fail', '上传失败');
            }
        }
        $activity->save();
        return redirect(action('ActivityController@index'))->with('success','上传成功');

    }

    public function update(Request $request, $id)
    {
        $user=Auth::user();
        $activity= Activity::find($id);
        $activity->content=$request->get('content');
        $activity->collection_id=$request->get('collection_id');
        $activity->cate_id=$request->get('cate_id');
        $activity->address=$request->get('address');
        $activity->start_time=$request->get('start_time');
        $activity->end_time=$request->get('end_time');
        $activity->title=$request->get('title');
        $activity->type=$request->get('type');
        $activity->user_id=$user->id;
        $activity->address=$request->get('address');
        if($request->has('img_url_top') && $file = $request->file('img_url_top')){
            if ($file->isValid()) {
                $img_path = $file->store('activities_image');
                $activity->img_url_top=$img_path;
            }
        }
        if($request->has('img_url_bottom') && $file = $request->file('img_url_bottom')){
            if ($file->isValid()) {
                $img_path = $file->store('activities_image');
                $activity->img_url_bottom=$img_path;
            }
            else {
                return redirect(route('activities'))->with('fail', '上传失败');
            }
        }
        $activity->save();
        return redirect(action('ActivityController@index'))->with('success','更新成功');

    }

        public function edit($id)
    {
        $activity=Activity::find($id);
        $collections=Collection::all();
        $categories=Category::all();
        return view('activities.edit',compact('activity','collections','categories','id'));
    }


}

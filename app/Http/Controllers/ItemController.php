<?php

namespace App\Http\Controllers;

use App\Item;
use App\ItemUser;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function index()
    {
        $items=Item::all();
        return view('item.index',compact('items'));
    }

    public function create()
    {
        return view('item.create');
    }

    public function buy(Request $request){
        $user=Auth::user();
        $item=Item::find($request->item_id);
        $user->point=$user->point-$item->point;
        $user->money=$user->money-$item->money;
        $user->save();
        //$item_user=ItemUser::where('user_id',$user->id)->where('item_id',$item->id)->get();

    }

    public function all(){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item=new Item();
        $item->name=$request->name;
        $item->description=$request->description;
        $item->point=$request->point;
        $item->money=$request->money;
        if($request->has('image_url') && $file = $request->file('image_url')){
            if ($file->isValid()) {
                $img_path = $file->store('items_image');
                $item->image_url=$img_path;
            }
        }
        $item->save();
        return redirect(action('ItemController@index'))->with('success','更新成功');


        /* $collection = new Collection();
         $collection->name = $request->get('name');
         $collection->save();
         return redirect('collections')->with('success','已添加类别');*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item=Item::find($id);
        return view('item.edit',compact('item','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item=Item::find($id);
        $item->name=$request->name;
        $item->description=$request->description;
        $item->point=$request->point;
        $item->money=$request->money;
        if($request->has('image_url') && $file = $request->file('image_url')){
            if ($file->isValid()) {
                $img_path = $file->store('items_image');
                $item->image_url=$img_path;
            }
        }
        $item->save();
        return redirect(action('ItemController@index'))->with('success','更新成功');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}

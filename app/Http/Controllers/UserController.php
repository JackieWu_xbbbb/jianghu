<?php

namespace App\Http\Controllers;

use App\Chatkit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function api_register(Request $request,Chatkit $chatkit){


        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'required|string|min:10|max:10|unique:users',
            'email' => 'string|email|max:255|unique:users',
        ]);
        if($validator->fails()){
            return response()->json(['message' => $validator->errors()],403);
        }

        User::create([
            'name' => $request->get('name'),
            'password' => Hash::make($request->get('password')),
            'phone' => $request->get('phone'),
            'email'=> $request->has("email") ,
            'chatkit_id'=>$request->get('phone')
        ]);
        $chatkit->createUser([
            'id' => $request->get('phone'),
            'name' =>$request->get('name'),
        ]);
        return response()->json(['message' => 'success'],200);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hobby extends Model
{
    protected $fillable = [
        'content','user_id','image_url','collection_id','cate_id'
    ];


    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function likes(){
        return $this->hasMany('App\Like');
    }

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    public $incrementing = false;
    protected $fillable = [
        'id', 'name', 'created_by_id', 'private',
    ];
}

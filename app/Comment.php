<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'content','user_id','hobby_id','target_user'
    ]; //

    public function hobbies()
    {
        return $this->belongsTo('App\Hobby');
    }
    public function users(){
        return $this->belongsTo("App\User");
    }
}

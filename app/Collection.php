<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $fillable = [
        'name'
    ]; ////

    public function hobbies()
    {
        return $this->hasMany('App\Hobby');
    }

    public function categories()
    {
        return $this->hasMany('App\Category');
    }
}

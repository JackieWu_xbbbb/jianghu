<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name','collection_id'
    ];//

    public function hobbies()
    {
        return $this->hasMany('App\Hobby');
    }

    public function collection(){
        return $this->belongsTo('App\Collection');
    }
}

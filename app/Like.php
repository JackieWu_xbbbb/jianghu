<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = [
        'user_id','hobby_id',
    ];//

    public function hobbies()
    {
        return $this->belongsTo('App\Hobby');
    }
    public function users(){
        return $this->belongsTo("App\User");
    }
}

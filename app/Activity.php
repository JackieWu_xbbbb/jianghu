<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = [
        'content','collection_id','cate_id','address','start_time','end_time','img_url_top','img_url_bottom','type','title','user_id'
    ];


    public function user(){
        return $this->belongsTo('App\User');
    }
}

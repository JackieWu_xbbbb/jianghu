<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentController extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string("content");
            $table->unsignedInteger("user_id");
            $table->foreign('user_id')->references("id")->on('users');
            $table->unsignedInteger("hobby_id");
            $table->foreign('hobby_id')->references("id")->on('hobbies');
            $table->unsignedInteger("target_user");
            $table->foreign('target_user')->references("id")->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}

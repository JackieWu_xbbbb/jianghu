<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('content');
            $table->string('title');
            $table->unsignedInteger("user_id");
            $table->foreign('user_id')->references("id")->on('users');
            $table->unsignedInteger("collection_id");
            $table->foreign('collection_id')->references("id")->on('collections');
            $table->unsignedInteger("cate_id");
            $table->foreign('cate_id')->references("id")->on('categories');
            $table->string('address');
            $table->date('start_time');
            $table->date('end_time');
            $table->string('img_url_top');
            $table->string('img_url_bottom');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}

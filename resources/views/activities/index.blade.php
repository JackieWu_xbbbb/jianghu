@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('activities.activity_tab')
        </div>
        <hr>
        @include('layouts.feedback')
        <table class="table table-bordered table-hover text-center">
            <thead>
            <tr>
                <td>
                    <h1>ID</h1>
                </td>
                <td>
                    <h1>标题</h1>
                </td>
                <td>
                    <h1>内容</h1>
                </td>
                <td>
                    <h1>地址</h1>
                </td>
                <td>
                    <h1>类型</h1>
                </td>
                <td>
                    <h1>图片</h1>
                </td>
                <td>
                    <h1>操作</h1>
                </td>
            </tr>
            </thead>
            @foreach($activities as $activity)
                <tr>
                    <td><h2>{{$activity['id']}}</h2></td>
                    <td><h4>{{$activity['title']}}</h4></td>
                    <td><p>{{$activity['content']}}</p></td>
                    <td><p>{{$activity['address']}}</p></td>
                    <td><p>{{$activity['type']}}</p></td>
                    <td>
                        <img src="storage/{{$activity['img_url_top']}}" style="width:100px;height:100px">
                        <img src="storage/{{$activity['img_url_bottom']}}" style="width:100px;height:100px">
                    </td>

                    <td>
                        <div class="btn-group-vertical d-flex">
                            <a href="{{action('ActivityController@edit', $activity['id'])}}" class="btn btn-info">编辑</a>
                            <form action="{{action('ActivityController@destroy',$activity['id'])}}" class="w-100" method="post">
                                @csrf
                                <input name="_method" type="hidden" value="DELETE">
                                <button onclick="return confirm('确定删除吗？')" type="submit" class="btn btn-danger">删除</button>
                            </form>
                        </div>
                    </td>
                </tr>

            @endforeach

        </table>
    </div>

@endsection

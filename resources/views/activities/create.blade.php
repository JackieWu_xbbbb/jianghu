<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
@extends('layouts.app')

@section('content')

    @include('layouts.feedback')
    @include('activities.activity_tab')
    <div class="container-fluid mt-2 mb-5" style="background-color: white">
            <div class="card">

                <div class="card-footer">
                    <form method="post" action="{{ action('ActivityController@store') }}" enctype="multipart/form-data">
                    @csrf

                            <div class="mb-3">
                                <div class="mb-2">
                                    <span class="text-left"><b>顶部图片上传</b></span>
                                </div>
                                <div class="bg-white row pl-3 pr-3 pt-2 pb-2">
                                    <div class="w-100">
                                        <input type="file" class="border-0 w-100 form-control {{ $errors->has('img_url_top') ? ' is-invalid' : '' }}" name="img_url_top" id="img_url_top"
                                               required>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-3">
                                <div class="mb-2">
                                    <span class="text-left"><b>底部图片上传</b></span>
                                </div>
                                <div class="bg-white row pl-3 pr-3 pt-2 pb-2">
                                    <div class="w-100">
                                        <input type="file" class="border-0 w-100 form-control {{ $errors->has('img_url_bottom') ? ' is-invalid' : '' }} " name="img_url_bottom" id="img_url_bottom"
                                               required>
                                    </div>
                                </div>
                            </div>

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">标题</label>
                            <div class="col-md-6">
                                    <input type="text" class="border-0 w-100 form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" id="title"
                                           required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">地址</label>
                            <div class="col-md-6">
                                <input type="text" class="border-0 w-100 form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" id="address"
                                       required >
                            </div>
                        </div>




                        <div class="form-group row">
                            <label for="start_time" class="col-md-4 col-form-label text-md-right">开始时间</label>
                            <div class="col-md-6">
                                    <input type="date" class="border-0 w-100 form-control {{ $errors->has('start_time') ? ' is-invalid' : '' }}" name="start_time" id="start_time"
                                           required >

                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="end_time" class="col-md-4 col-form-label text-md-right">结束时间</label>
                            <div class="col-md-6">
                                    <input type="date" class="border-0 form-control {{ $errors->has('end_time') ? ' is-invalid' : '' }} w-100" name="end_time" id="end_time"
                                           required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">种类</label>
                            <div class="col-md-6">
                                <select class="form-control {{ $errors->has('type') ? ' is-invalid' : '' }}"
                                        name="type" id="type" required>
                                    <option>类别</option>
                                    <option value='0'>送东西</option>
                                    <option value='1'>找活动</option>
                                    <option value='2'>约活动</option>
                                    <option value='3'>服务</option>
                                </select>
                            </div>
                        </div>

                            <div class="form-group row">
                                <label for="collection_id" class="col-md-4 col-form-label text-md-right">上层类别</label>
                                <div class="col-md-6">
                                    <select class="form-control {{ $errors->has('collection_id') ? ' is-invalid' : '' }}"
                                            name="collection_id" id="collection_id" required>
                                        <option>类别</option>
                                        @foreach($collections as $collection)
                                            <option value="{{$collection->id}}">{{$collection->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="cate_id" class="col-md-4 col-form-label text-md-right">类别</label>
                                <div class="col-md-6">
                                    <select class="form-control {{ $errors->has('cate_id') ? ' is-invalid' : '' }}"
                                            name="cate_id" id="cate_id" required>
                                        <option>请选择类别</option>
                                    </select>
                                </div>
                            </div>

                            <textarea  type="text" class="form-control {{ $errors->has('content') ? ' is-invalid' : '' }} mb-2" name="content" id="content"
                                       placeholder="填写内容" required>

                            </textarea>

                            <button type="submit" class="btn btn-info btn-block">上传</button>
                    </form>
                </div>
            </div>

    </div>

@endsection

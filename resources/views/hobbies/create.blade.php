<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
@extends('layouts.app')

@section('content')

    @include('layouts.feedback')
    @include('hobbies.hobby_tab')
    <div class="container-fluid mt-2 mb-5">

        <div class="card">

                <div class="card-footer">
                    <form method="post" action="{{ action('HobbyController@store') }}" enctype="multipart/form-data">
                    @csrf

                            <div class="mb-3">
                                <div class="mb-2">
                                    <span class="text-left"><b>图片上传（可上传多张）</b></span>
                                </div>
                                <div class="bg-white row pl-3 pr-3 pt-2 pb-2">
                                    <div class="w-100">
                                        <input type="file" class="form-control {{ $errors->has('image_url') ? ' is-invalid' : '' }} border-0 w-100" name="image_url[]" id="image_url" multiple
                                               required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="collection_id" class="col-md-4 col-form-label text-md-right">上层类别</label>
                                <div class="col-md-6">
                                    <select class="form-control {{ $errors->has('collection_id') ? ' is-invalid' : '' }}"
                                            name="collection_id" id="collection_id" required>
                                        <option>类别</option>
                                        @foreach($collections as $collection)
                                            <option value="{{$collection->id}}">{{$collection->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="cate_id" class="col-md-4 col-form-label text-md-right">上层类别</label>
                                <div class="col-md-6">
                                    <select class="form-control {{ $errors->has('cate_id') ? ' is-invalid' : '' }}"
                                            name="cate_id" id="cate_id" required>
                                        <option>请选择类别</option>
                                    </select>
                                </div>
                            </div>

                            <textarea  type="text" class="form-control {{ $errors->has('content') ? ' is-invalid' : '' }} mb-2" name="content" id="content"
                                       placeholder="填写内容" required>

                            </textarea>

                            <button type="submit" class="btn btn-info btn-block">上传</button>
                    </form>
                </div>
            </div>

    </div>

@endsection

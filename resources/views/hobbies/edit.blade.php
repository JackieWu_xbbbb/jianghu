<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
@extends('layouts.app')

@section('content')

    @include('layouts.feedback')
    @include('hobbies.hobby_tab')
    <script src="{{asset('/js/get_cate.js')}}"></script>
    <div class="container-fluid mt-2 mb-5">
        <div class="card">

            <div class="card-footer">
                <form method="post" action="{{ route('hobbies.update', $id) }}" enctype="multipart/form-data">

                    @csrf
                    @include('layouts.feedback')
                    <input name="_method" type="hidden" value="PATCH">
                    <div class="mb-3">
                        <div class="mb-2">
                            <span class="text-left"><b>图片上传（可上传多张）</b></span>
                        </div>
                        <div class="bg-white row pl-3 pr-3 pt-2 pb-2">
                            <div class="w-100">
                                <input type="file" class="border-0 form-control {{ $errors->has('image_url') ? ' is-invalid' : '' }} w-100" name="image_url[]" id="image_url" multiple
                                       >
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="collection_id" class="col-md-4 col-form-label text-md-right">上层类别</label>
                        <div class="col-md-6">
                            <select class="form-control {{ $errors->has('collection_id') ? ' is-invalid' : '' }}"
                                    name="collection_id" id="collection_id" required>
                                <option>类别</option>
                                @foreach($collections as $collection)
                                    <option value="{{$collection->id}}" @if($collection->id == $hobby->collection_id) selected @endif >{{$collection->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="cate_id" class="col-md-4 col-form-label text-md-right">上层类别</label>
                        <div class="col-md-6">
                            <select class="form-control {{ $errors->has('cate_id') ? ' is-invalid' : '' }}"
                                    name="cate_id" id="cate_id" required>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" @if($category->id == $hobby->collection_id) selected @endif >{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <textarea  type="text" class="form-control {{ $errors->has('content') ? ' is-invalid' : '' }} mb-2" name="content" id="content"
                               placeholder="填写内容" required>{{$hobby->content}}</textarea>

                    <button type="submit" class="btn btn-info btn-block">上传</button>
                </form>
            </div>
        </div>

    </div>

@endsection

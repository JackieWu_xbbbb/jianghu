@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('hobbies.hobby_tab')
        </div>
        <hr>
        @include('layouts.feedback')
        <table class="table table-bordered table-hover text-center">
            <thead>
            <tr>
                <td>
                    <h1>ID</h1>
                </td>
                <td>
                    <h1>内容</h1>
                </td>
                <td>
                    <h1>图片</h1>
                </td>
                <td>
                    <h1>操作</h1>
                </td>
            </tr>
            </thead>
            @foreach($hobbies as $hobby)
                <tr>
                    <td><h2>{{$hobby['id']}}</h2></td>
                    <td><p>{{$hobby['content']}}</p></td>

                    <td>
                        @foreach(explode('|',$hobby['image_url']) as $img)
                            <h5>{{$hobby['img_url']}}</h5>
                            <img src="storage/{{$img}}"
                                 style="width:100px;height:100px">
                        @endforeach
                    </td>

                    <td>
                        <div class="btn-group-vertical d-flex">
                            <a href="{{action('HobbyController@edit', $hobby['id'])}}" class="btn btn-info">编辑</a>
                            <form action="{{action('HobbyController@destroy',$hobby['id'])}}" class="w-100" method="post">
                                @csrf
                                <input name="_method" type="hidden" value="DELETE">
                                <button onclick="return confirm('确定删除吗？')" type="submit" class="btn btn-danger">删除</button>
                            </form>
                        </div>
                    </td>
                </tr>

            @endforeach

        </table>
    </div>

@endsection

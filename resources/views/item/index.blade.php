@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('item.item_tab')
        </div>
        <hr>
        @include('layouts.feedback')
        <table class="table table-bordered table-hover text-center">
            <thead>
            <tr>
                <td>
                    <h1>ID</h1>
                </td>
                <td>
                    <h1>名称</h1>
                </td>
                <td>
                    <h1>描述</h1>
                </td>
                <td>
                    <h1>积分</h1>
                </td>
                <td>
                    <h1>银两</h1>
                </td>
                <td>
                    <h1>图片</h1>
                </td>
                <td>
                    <h1>操作</h1>
                </td>
            </tr>
            </thead>
            @foreach($items as $item)
                <tr>
                    <td><h2>{{$item['id']}}</h2></td>
                    <td><p>{{$item['name']}}</p></td>
                    <td><p>{{$item['description']}}</p></td>
                    <td><p>{{$item['point']}}</p></td>
                    <td><p>{{$item['money']}}</p></td>
                    <td>
                        <img src="storage/{{$item['image_url']}}" style="width:100px;height:100px">
                    </td>

                    <td>
                        <div class="btn-group-vertical d-flex">
                            <a href="{{action('ItemController@edit', $item['id'])}}" class="btn btn-info">编辑</a>
                            <form action="{{action('ItemController@destroy',$item['id'])}}" class="w-100" method="post">
                                @csrf
                                <input name="_method" type="hidden" value="DELETE">
                                <button onclick="return confirm('确定删除吗？')" type="submit" class="btn btn-danger">删除</button>
                            </form>
                        </div>
                    </td>
                </tr>

            @endforeach

        </table>
    </div>

@endsection

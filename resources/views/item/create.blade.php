<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
@extends('layouts.app')

@section('content')

    @include('layouts.feedback')
    @include('item.item_tab')
    <div class="container-fluid mt-2 mb-5">

        <div class="card">

                <div class="card-footer">
                    <form method="post" action="{{ action('ItemController@store') }}" enctype="multipart/form-data">
                    @csrf

                            <div class="mb-3">
                                <div class="mb-2">
                                    <span class="text-left"><b>图片上传</b></span>
                                </div>
                                <div class="bg-white row pl-3 pr-3 pt-2 pb-2">
                                    <div class="w-100">
                                        <input type="file" class="form-control {{ $errors->has('image_url') ? ' is-invalid' : '' }} border-0 w-100" name="image_url" id="image_url"
                                               required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">名称</label>
                                <div class="col-md-6">
                                    <input type="text" class="border-0 w-100 form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name"
                                           required >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">描述</label>
                                <div class="col-md-6">
                                    <input type="text" class="border-0 w-100 form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="description"
                                           required >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="point" class="col-md-4 col-form-label text-md-right">积分</label>
                                <div class="col-md-6">
                                    <input type="number" class="border-0 w-100 form-control {{ $errors->has('points') ? ' is-invalid' : '' }}" name="point" id="point"
                                           required >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="money" class="col-md-4 col-form-label text-md-right">银两</label>
                                <div class="col-md-6">
                                    <input type="number" class="border-0 w-100 form-control {{ $errors->has('money') ? ' is-invalid' : '' }}" name="money" id="money"
                                           required >
                                </div>
                            </div>



                            <button type="submit" class="btn btn-info btn-block">上传</button>
                    </form>
                </div>
            </div>

    </div>

@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('collections.collection_tab')
        </div>
        <hr>
        @include('layouts.feedback')
        <table class="table table-bordered table-hover text-center">
            <thead>
            <tr>
                <td>
                    <h1>ID</h1>
                </td>
                <td>
                    <h1>名称</h1>
                </td>
                <td>
                    <h1>操作</h1>
                </td>
            </tr>
            </thead>
            @foreach($collections as $collection)
                <tr>
                    <td><h2>{{$collection['id']}}</h2></td>
                    <td><h2>{{$collection['name']}}</h2></td>

                    <td>
                        <div class="btn-group-vertical d-flex">
                            <a href="{{action('CollectionController@show', $collection['id'])}}" class="btn btn-info">显示</a>
                            <a href="{{action('CollectionController@edit', $collection['id'])}}" class="btn btn-info">编辑</a>
                            <form action="{{action('CollectionController@destroy',$collection['id'])}}" class="w-100" method="post">
                                @csrf
                                <input name="_method" type="hidden" value="DELETE">
                                <button onclick="return confirm('确定删除吗？')" type="submit" class="btn btn-danger">删除</button>
                            </form>
                        </div>
                    </td>
                </tr>

            @endforeach

        </table>
    </div>

@endsection

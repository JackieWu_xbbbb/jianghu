$(function(){

    $("#collection_id").change(function(){

        var objectModel = {};
        var   value = $(this).val();
        var   type = $(this).attr('id');
        objectModel[type] =value;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:"/hobbies/create/get_cate_id_by_collection_id",
            type:"post",
            dataType:"json",
            data:objectModel,
            timeout:30000,
            success:function(data){

                $("#cate_id").empty();
                var count = data.length;
                var i = 0;
                var content="";
                for(i=0;i<count;i++){
                    content+="<option value='"+data[i].id+"'>"+data[i].name+"</option>";
                }
                $("#cate_id").append(content);

            }
        });
        return false;
    });



})




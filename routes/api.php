<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/user/register', 'UserController@api_register');

Route::middleware('auth:api')->post('/hobby/post','HobbyController@post');
Route::get('/hobby/all','HobbyController@all');


Route::middleware('auth:api')->post('/like/post','LikeController@post');
Route::middleware('auth:api')->post('/comment/post','CommentController@post');


Route::middleware('auth:api')->post('/activity/post','ActivityController@post');
Route::get('/activity/all','ActivityController@all');
Route::get('/activity/get_by_cate/{id}/{type}','ActivityController@get_activity_by_cate_id');
Route::get('/activity/get_by_type/{type}','ActivityController@get_activity_by_type');
Route::get('/activity/search_service/{word}','ActivityController@search_service');
Route::get('/collection/all','CollectionController@all');
Route::get('/category/all','CategoryController@all');
Route::middleware('auth:api')->get('/activity/get_by_joined','ActivityController@get_joined_activity');
Route::middleware('auth:api')->post('/activity/join','ActivityController@join');
Route::middleware('auth:api')->get('/activity/check/{user_id}/{activity_id}','ActivityController@check_joined_activity');


Route::middleware('auth:api')->post('/chat/create_one_to_one_chat','ChatController@create_one_to_one_chat');
Route::middleware('auth:api')->get('/chat/get_chat','ChatController@get_all_chat');




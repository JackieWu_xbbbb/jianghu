<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/categories/order','CategoryController@order')->middleware('admin')->name('categories.order');

Route::resource('categories', 'CategoryController')->middleware('admin');


Route::resource('collections', 'CollectionController')->middleware('admin');
Route::resource('hobbies', 'HobbyController')->middleware('admin');
Route::post('hobbies/create/get_cate_id_by_collection_id','CategoryController@get_cate_by_collection_id');

Route::resource('activities', 'ActivityController')->middleware('admin');
Route::resource('items', 'ItemController')->middleware('admin');